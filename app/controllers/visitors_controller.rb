class VisitorsController < ApplicationController
  def index
  end

  def new
    if params[:set_locale]
        redirect_to root_url(locale: params[:set_locale])
    else
        @visitor= Visitor.new
    end
  end

  def create
    @visitor = Visitor.new(params[:visitor])
    @visitor.request = request
    if @visitor.deliver
      flash[:success] = I18n.t('visitors.confirmations.success')
      #render :new
      redirect_to :action => 'new', :anchor => 'contact'
    else
      flash.now[:error] = I18n.t('visitors.denied.error')
      render :new
    end
  end
end
