class Visitor < MailForm::Base
  attribute :name
  attribute :email
  attribute :message
  attribute :nickname,  :captcha  => true

  validates :name, :email, :message,  presence: true

  validates :name, length: { maximum: 50 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true,
                    length: { maximum: 50 },
                    format: { with: VALID_EMAIL_REGEX }

  validates :message, presence: true,
                      length: { minimum: 4, maximum: 500 }


  # in ActionMailer accepts.
  def headers
    {
      :subject => "Contato Web Page( MG Fine Products )",
      :to => "info@mgfineproducts.com",
      :from => %("#{name}" <#{email}>)
    }
  end

end
