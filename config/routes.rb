Rails.application.routes.draw do

  scope '(:locale)' do
    resources "visitors", only: [:new, :create]
    root to: 'visitors#new', via: :all
  end

end
